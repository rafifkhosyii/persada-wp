@extends('layouts.app')
@section('title','Dashboard')
@section('toolbar-status','false')

@section('navbar')
@include('layouts.navbar.navbar')
@endsection

@section('sidebar')
@include('layouts.navbar.sidebar')
@endsection

@section('content')
<div class="row h-100">
	<div class="col-lg-12 align-self-center">
		<div class="row justify-content-center align-items-center">
			<div class="col-lg-8 mb-10">
				<div class="text-center">
					<img src="{{asset('sense')}}/media/logos/logo-persada.png" class="mw-100 mh-200px" alt="">
				</div>
			</div>
			<div class="col-lg-8">
				<div class="row">
					<div class="col-xl-6">
						<a href="#" class="card bg-primary hoverable card-xl-stretch mb-xl-8">
							<div class="card-body">
								<i class="fa-solid fa-users text-white fs-2x"></i>       
								<div class="text-gray-100 fw-bold fs-2 mt-5">           
									 {{$totalPemain}}                  
								</div>
								<div class="fw-semibold text-gray-100">
									Total Karyawan        
								</div>
							</div>
						</a>
					</div>
					<div class="col-xl-6">
						<a href="#" class="card bg-info hoverable card-xl-stretch mb-xl-8">
							<div class="card-body">
								<i class="fa-solid fa-list-check text-white fs-2x"></i>       
								<div class="text-gray-100 fw-bold fs-2 mt-5">           
									 {{$totalKriteria}}                  
								</div>
								<div class="fw-semibold text-gray-100">
									Total Kriteria        
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	@endsection
	