@extends('layouts.app')
@section('title','Karyawan')

@section('navbar')
@include('layouts.navbar.navbar')
@endsection

@section('sidebar')
@include('layouts.navbar.sidebar')
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-12">
        <div class="row justify-content-center mt-9">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="d-flex align-items-center gap-2 mb-3 mb-md-0">
                            <div class="d-flex align-items-center">
                                <span class="fs-7 fw-bolder text-dark pe-4 text-nowrap d-none d-lg-block">List Karyawan</span>
                            </div>
                        </div>
                        <div class="card-toolbar">
                            <a href="#kt_modal_tambah_karyawan" data-bs-toggle="modal" class="btn btn-sm btn-primary btn_tambah_karyawan"><i class="fa-solid fa-plus me-1"></i>Tambah Karyawan</a>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table align-middle table-striped border table-rounded gy-5" id="kt_table_karyawan">
                                    <thead class="bg-primary">
                                        <tr class="fw-bold fs-7 text-white text-uppercase">
                                            <th class="text-center w-50px">#</th>
                                            <th class="">Nama Karyawan</th>
                                            <th class="w-100px">NIP</th>
                                            <th class="w-100px text-center">#</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fs-7">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>

@include('player.add.modal-hapus-karyawan')
@include('player.add.modal-edit-karyawan')
@include('player.add.modal-tambah-karyawan')

<script>
    $(document ).ready(function() {
        
        window.tableDisc  = $('#kt_table_karyawan')
        .DataTable({
            processing: true,
            serverSide: true,
            retrieve: true,
            deferRender: true,
            responsive: false,
            aaSorting : [],
            ajax: {
                url : "{{route('player.get-table-player')}}"
            },
            language: {
                "lengthMenu": "Show _MENU_",
                "emptyTable" : "Tidak ada data terbaru 📁",
                "zeroRecords": "Data tidak ditemukan 😞",
            },
            dom:
            "<'row mb-2'" +
            "<'col-12 col-lg-6 d-flex align-items-center justify-content-start'l>" +
            "<'col-12 col-lg-6 d-flex align-items-center justify-content-lg-end justify-content-start 'f>" +
            ">" +
            
            "<'table-responsive'tr>" +
            
            "<'row'" +
            "<'col-12 col-lg-5 d-flex align-items-center justify-content-center justify-content-lg-start'i>" +
            "<'col-12 col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-end'p>" +
            ">",
            
            columns: [
            { data: 'DT_RowIndex'},
            { data: 'nama_karyawan'},
            { data: 'nip'},
            { data: 'action'},
            ],
            columnDefs: [
            {
                targets: 0,
                searchable : false,
                className: 'text-center',
            },
            {
                targets: -1,
                orderable : false,
                searchable : false,
                className : 'text-center',
            },
            ],
        });

        $('body').on('click', '.btn_tambah_karyawan', function () {
            var form_tambah = $('#kt_modal_tambah_karyawan_form')
            form_tambah.trigger("reset");
        });

        $("#kt_modal_tambah_karyawan_form").validate({
            messages: {
                nama_karyawan: {
                    required: "<span class='fw-semibold fs-8 text-danger'>Nama Karyawan wajib diisi</span>",
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_tambah_karyawan_form').serialize(),
                    url: '{{route("player.store")}}',
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_tambah_karyawan_cancel').click();
                        var oTable = $('#kt_table_karyawan').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });

        $('table').on('click', '.btn_hapus_karyawan', function () {
            var id = $(this).data('id')
            var form_hapus = $('#kt_modal_hapus_karyawan_form')
            form_hapus.trigger("reset");
            $.get(`{{url('')}}/persada-player/get-data/data/player/${id}`, function (data) {
                form_hapus.find("input[name='player_id']").val(id)
            })
        });

        $("#kt_modal_hapus_karyawan_form").validate({
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_hapus_karyawan_form').serialize(),
                    url: '{{route("player.destroy")}}',
                    type: "DELETE",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_hapus_karyawan_cancel').click();
                        var oTable = $('#kt_table_karyawan').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });

        $('table').on('click', '.btn_edit_karyawan', function () {
            var id = $(this).data('id')
            var nama = $(this).data('nama')
            var form_edit = $('#kt_modal_edit_karyawan_form')
            form_edit.trigger("reset");
            $.get(`{{url('')}}/persada-player/get-data/data/player/${id}`, function (data) {
                form_edit.find("input[name='karyawan_id']").val(id)
                form_edit.find("input[name='nama_karyawan']").val(nama)
            })
        });

        $("#kt_modal_edit_karyawan_form").validate({
            messages: {
                nama_karyawan: {
                    required: "<span class='fw-semibold fs-8 text-danger'>Nama Karyawan wajib diisi</span>",
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_edit_karyawan_form').serialize(),
                    url: '{{route("player.update")}}',
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_edit_karyawan_cancel').click();
                        var oTable = $('#kt_table_karyawan').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });
        
    });
</script>
@endsection
