<div class="row mb-6">
    <div class="col-lg-12 mb-3">
        <label class="d-flex align-items-center fs-6 form-label mb-2">
            <span class="required fw-bold">Nama Karyawan</span>
        </label>
        <input type="text" class="form-control form-control-solid" placeholder="" required name="nama_karyawan">
        <div class="fv-plugins-message-container invalid-feedback"></div>
    </div>
</div>