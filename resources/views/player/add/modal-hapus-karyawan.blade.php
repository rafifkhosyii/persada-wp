<div class="modal fade" id="kt_modal_hapus_karyawan" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content rounded">
            <div class="modal-header pb-0 border-0 justify-content-end">
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
                <form class="form" id="kt_modal_hapus_karyawan_form" enctype="multipart/form-data">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="player_id">
                    <div class="row justify-content-center mb-9">
                        <div class="col-xl-8">
                            <div class="text-center mb-6">
                                <i class="fas fa-exclamation-circle text-danger fs-3x"></i>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-xl-12 text-center">
                                    <span class="fs-1 fw-bolder text-dark d-block mb-2">Periksa Kembali!</span>
                                    <span class="fs-7 fw-semibold text-muted">Apa yakin ingin menghapus data ini? Data yang dihapus tidak dapat dikembalikan lagi</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="reset" id="kt_modal_hapus_karyawan_cancel" class="btn btn-light me-3 btn-sm w-xl-200px" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" id="kt_modal_hapus_karyawan_submit" class="btn btn-danger btn-sm w-xl-200px">
                            <span class="indicator-label">Hapus</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

