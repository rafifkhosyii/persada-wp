
<div class="modal fade" id="kt_modal_tambah_karyawan" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-650px">
		<div class="modal-content">
			<div class="modal-header py-3">
				<h5 class="fw-bolder">Tambah Karyawan Baru</h5>
				<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
					<i class="fas fa-times"></i>
				</div>
			</div>
			<div class="modal-body mx-5 mx-lg-15 my-7">
				<form id="kt_modal_tambah_karyawan_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" enctype="multipart/form-data">
					@csrf
					<div class="scroll-y me-n10 pe-10" id="kt_modal_tambah_karyawan_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_tambah_karyawan_header" data-kt-scroll-wrappers="#kt_modal_tambah_karyawan_scroll" data-kt-scroll-offset="300px">
					@include('player.add.form-karyawan')
				</div>
					<div class="text-center mt-6">
						<button type="reset" id="kt_modal_tambah_karyawan_cancel" class="btn btn-sm btn-light me-3 w-lg-200px" data-bs-dismiss="modal">Cancel</button>
						<button type="submit" id="kt_modal_tambah_karyawan_submit" class="btn btn-sm btn-primary w-lg-200px">
							<span class="indicator-label">Simpan</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>