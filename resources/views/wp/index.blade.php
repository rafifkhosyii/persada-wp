@extends('layouts.app')
@section('title','List Penilaian')

@section('navbar')
@include('layouts.navbar.navbar')
@endsection

@section('sidebar')
@include('layouts.navbar.sidebar')
@endsection

@section('content')
<div class="row justify-content-center mt-9">
    <div class="col-lg-12">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="d-flex align-items-center gap-2 mb-3 mb-md-0">
                            <div class="d-flex align-items-center">
                                <span class="fs-7 fw-bolder text-dark pe-4 text-nowrap d-none d-lg-block">List Penilaian Karyawan Terbaik</span>
                            </div>
                        </div>
                        <div class="card-toolbar">
                            @role('administrator')
                            <a href="{{ route('wp.create')}}" class="btn btn-sm btn-primary"><i class="fa-solid fa-plus me-1"></i>Periode Penilaian Baru</a>
                            @endrole
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table align-middle table-striped border table-rounded gy-5" id="kt_table_wp">
                                    <thead class="bg-primary">
                                        <tr class="fw-bold fs-7 text-white text-uppercase">
                                            <th class="text-center w-50px">#</th>
                                            <th class="text-center w-150px">Tgl Test</th>
                                            <th class="text-center w-150px">Kode Test</th>
                                            <th class="">Karyawan Terbaik</th>
                                            <th class="w-100px">NIP</th>
                                            <th class="w-150px">Nilai</th>
                                            <th class="w-100px text-center">#</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fs-7">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>

@include('wp.add.modal-hapus-wp')

<script>
    $(document ).ready(function() {
        
        window.tableDisc  = $('#kt_table_wp')
        .DataTable({
            processing: true,
            serverSide: true,
            retrieve: true,
            deferRender: true,
            responsive: false,
            aaSorting : [],
            ajax: {
                url : "{{route('wp.get-table-wp')}}"
            },
            language: {
                "lengthMenu": "Show _MENU_",
                "emptyTable" : "Tidak ada data terbaru 📁",
                "zeroRecords": "Data tidak ditemukan 😞",
            },
            dom:
            "<'row mb-2'" +
            "<'col-12 col-lg-6 d-flex align-items-center justify-content-start'l>" +
            "<'col-12 col-lg-6 d-flex align-items-center justify-content-lg-end justify-content-start 'f>" +
            ">" +
            
            "<'table-responsive'tr>" +
            
            "<'row'" +
            "<'col-12 col-lg-5 d-flex align-items-center justify-content-center justify-content-lg-start'i>" +
            "<'col-12 col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-end'p>" +
            ">",
            
            columns: [
            { data: 'DT_RowIndex'},
            { data: 'tanggal_test'},
            { data: 'kode_test'},
            { data: 'nama_karyawan'},
            { data: 'nip'},
            { data: 'nilai_v_karyawan_terbaik', searchable:false},
            { data: 'action'},
            ],
            columnDefs: [
            {
                targets: 0,
                searchable : false,
                className: 'text-center',
            },
            {
                targets: 1,
                searchable : false,
                className: 'text-center',
            },
            {
                targets: 2,
                className: 'text-center',
            },
            {
                targets: 3,
                className: 'fw-bold',
            },
            {
                targets: -1,
                orderable : false,
                searchable : false,
                className : 'text-center',
            },
            ],
        });

        $('table').on('click', '.btn_hapus_wp', function () {
            var id = $(this).data('id')
            var form_hapus = $('#kt_modal_hapus_wp_form')
            form_hapus.trigger("reset");
            $.get(`{{url('')}}/persada-player/get-data/data/player/${id}`, function (data) {
                form_hapus.find("input[name='wp_id']").val(id)
            })
        });

        $("#kt_modal_hapus_wp_form").validate({
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_hapus_wp_form').serialize(),
                    url: '{{route("wp.destroy")}}',
                    type: "DELETE",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_hapus_wp_cancel').click();
                        var oTable = $('#kt_table_wp').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });
        
    });
</script>
@endsection
