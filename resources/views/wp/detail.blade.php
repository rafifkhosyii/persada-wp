@extends('layouts.app')
@section('title','Penilaian Karyawan Terbaik')
@section('toolbar-status','false')

@section('navbar')
@include('layouts.navbar.navbar')
@endsection

@section('sidebar')
@include('layouts.navbar.sidebar')
@endsection

@section('content')
<div class="row justify-content-center mt-9">
    <div class="col-lg-12">
        <div class="row justify-content-center">
            <div class="col-lg-12 mb-6">
                <div class="card card-flush">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="d-flex flex-column">
                                    <span class="fs-4 fw-bold text-dark d-block">{{ $getKaryawanTerbaik->kode_test }}</span>
                                    <span class="fs-7">Tanggal : {{ $getKaryawanTerbaik->tanggal_test }}</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="notice align-items-center d-flex bg-light-success rounded border-success border border-dashed p-6">
                                    <i class="fa-solid fa-user fs-4 me-4 text-success"></i>
                                    <div class="d-flex flex-stack flex-grow-1">
                                        <div class="fs-4">
                                            <span class="text-dark mb-0">Karyawan Terbaik : <b>{{ $getKaryawanTerbaik->nama_karyawan }}</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mb-6">
                <div class="card card-flush">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-lg-9 mb-6">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol rounded symbol-35px overflow-hidden me-5">
                                                <div class="symbol-label bg-light-primary">
                                                    <i class="fa-solid fa-id-badge text-primary fs-3"></i>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <span class="fs-4 fw-bold text-dark d-block">Data Penilaian</span>
                                                <span class="fs-7 text-muted">Pengisian Penilaian Tiap Karyawan PT. Persada</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 table-responsive mb-6">
                                <table class="table table-bordered table-striped border table-rounded gy-5" id="">
                                    <thead class="bg-primary text-white align-middle">
                                        <tr class="fw-bold fs-7 text-uppercase">
                                            <th class="text-center w-50px bg-black text-white">#</th>
                                            <th class="w-150px bg-black text-white text-start">Nama Karyawan</th>
                                            @foreach ($kriteriaDipakai as $kdnn)
                                            <th class="w-150px text-center">{{ $kdnn->nama_kriteria }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody class="fs-7">
                                        @php
                                        $index=1;
                                        @endphp
                                        @foreach ($karyawanDipakai as $kdn)
                                        <tr>
                                            <td class="text-center">{{ $index++ }}</td>
                                            <td>
                                                {{ $kdn->nama_karyawan }}
                                            </td>
                                            @foreach ($kriteriaDipakai as $kdnn2)
                                            @php
                                            $nilaiKaryawan = App\Models\WpPlayer::where('wp_id',$kdnn2->wp_id)->where('criteria_id',$kdnn2->criteria_id)->where('karyawan_id',$kdn->karyawan_id)->get();
                                            @endphp
                                            @foreach ($nilaiKaryawan as $nk)
                                            <td class="text-center">
                                                @if ( $nk->c_k_nilai == 1)
                                                Sangat Rendah (1)
                                                @elseif ( $nk->c_k_nilai == 2)
                                                Rendah (2)
                                                @elseif ( $nk->c_k_nilai == 3)
                                                Cukup (3)
                                                @elseif ( $nk->c_k_nilai == 4)
                                                Tinggi (4)
                                                @else
                                                Sangat Tinggi (5)
                                                @endif
                                            </td>
                                            @endforeach
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mb-6">
                <div class="card card-flush">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-lg-12 mb-6">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol rounded symbol-35px overflow-hidden me-5">
                                                <div class="symbol-label bg-light-primary">
                                                    <i class="fa-solid fa-book text-primary fs-3"></i>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <span class="fs-4 fw-bold text-dark d-block">Data Bobot Kriteria</span>
                                                <span class="fs-7 text-muted">Bobot Awal & Normalisasi</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mb-6">
                                <table class="table table-bordered table-striped border table-rounded gy-5" id="">
                                    <thead class="bg-primary text-white align-middle">
                                        <tr class="fw-bold fs-7 text-uppercase">
                                            <th class="text-center w-50px text-white">#</th>
                                            <th class="text-white text-start">Nama Kriteria</th>
                                            <th class="w-100px text-white text-center">Tipe</th>
                                            <th class="w-150px text-white text-center">Bobot Awal</th>
                                            <th class="w-200px text-white text-start">Normalisasi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fs-7">
                                        @php
                                        $index=1;
                                        @endphp
                                        @foreach ($kriteriaDipakai as $gkd)
                                        <tr>
                                            <td class="text-center">{{ $index++ }}</td>
                                            <td>
                                                {{ $gkd->nama_kriteria }}
                                            </td>
                                            <td class="text-center">
                                                @if ($gkd->tipe_kriteria < 0)
                                                <span class="badge badge-danger">Cost</span>
                                                @else
                                                <span class="badge badge-success">Benefit</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                {{ $gkd->bobot_kriteria }}
                                            </td>
                                            <td>
                                                {{ $gkd->normalisasi_kriteria }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mb-6">
                <div class="card card-flush">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-lg-12 mb-6">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol rounded symbol-35px overflow-hidden me-5">
                                                <div class="symbol-label bg-light-primary">
                                                    <i class="fa-solid fa-pen text-primary fs-3"></i>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <span class="fs-4 fw-bold text-dark d-block">Data Nilai S</span>
                                                <span class="fs-7 text-muted">Perhitungan Nilai S Karyawan</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 table-responsive mb-6">
                                <table class="table table-bordered table-striped border table-rounded gy-5" id="">
                                    <thead class="bg-primary text-white align-middle">
                                        <tr class="fw-bold fs-7 text-uppercase">
                                            <th class="text-center w-50px text-white">#</th>
                                            <th class="text-white w-150px text-start">Nama Pegawai</th>
                                            @php
                                            $noS=1;
                                            @endphp
                                            @foreach ($kriteriaDipakaiS as $gkds)
                                            <th class="w-100px text-white text-center">S{{ $noS++ }}</th>    
                                            @endforeach
                                            <th class="w-100px text-white text-center">Π Nilai S</th>
                                            <th class="w-100px text-white text-center">Σ Nilai S</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fs-7">
                                        @php
                                        $index=1;
                                        @endphp
                                        @foreach ($karyawanDipakaiS as $kdns)
                                        @php
                                            $powsumk = App\Models\NilaiV::where('wp_id',$id)->where('karyawan_id',$kdns->karyawan_id)->first();
                                        @endphp
                                        <tr>
                                            <td class="text-center">{{ $index++ }}</td>
                                            <td>
                                                {{ $kdns->nama_karyawan }}
                                                </td>
                                                @foreach ($kriteriaDipakaiS as $kdns2)
                                            @php
                                            $nilaiSKaryawan = App\Models\NilaiS::where('wp_id',$kdns2->wp_id)->where('criteria_id',$kdns2->criteria_id)->where('karyawan_id',$kdns->karyawan_id)->get();
                                            @endphp
                                            @foreach ($nilaiSKaryawan as $nsk)
                                            <td class="text-center">
                                                {{ $nsk->s_k_nilai }}
                                            </td>
                                            @endforeach
                                            @endforeach
                                            <td class="text-center">{{ $powsumk->pow_s_k_nilai }}</td>
                                            <td class="text-center">{{ $powsumk->sum_s_k_nilai }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 mb-6">
        <div class="card card-flush">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row align-items-center">
                            <div class="col-lg-12 mb-6">
                                <div class="d-flex align-items-center">
                                    <div class="symbol rounded symbol-35px overflow-hidden me-5">
                                        <div class="symbol-label bg-light-success">
                                            <i class="fa-solid fa-trophy text-success fs-3"></i>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="fs-4 fw-bold text-dark d-block">Data Nilai V</span>
                                        <span class="fs-7 text-muted">Perhitungan Nilai V & Rangking Karyawan</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-6">
                        <table class="table table-bordered table-striped border table-rounded gy-5" id="">
                            <thead class="bg-success text-white align-middle">
                                <tr class="fw-bold fs-7 text-uppercase">
                                    <th class="text-center w-50px text-white">#</th>
                                    <th class="text-white w-150px text-start">Nama Pegawai</th>
                                    <th class="w-100px text-white text-center">V</th>
                                </tr>
                            </thead>
                            <tbody class="fs-7">
                                @php
                                $index=1;
                                @endphp
                                @foreach ($nilaiVKaryawan as $gnv)
                                @php
                                $dataKaryawan = App\Models\Player::where('id',$gnv->karyawan_id)->first();
                                @endphp
                                <tr>
                                    <td class="text-center">{{ $index++ }}</td>
                                    <td>
                                        {{ $gnv->nama_karyawan }}
                                    </td>
                                    <td class="text-center">
                                        {{ $gnv->nilai_v_karyawan}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- </div>
</div> --}}
@endsection
