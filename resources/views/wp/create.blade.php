@extends('layouts.app')
@section('title','Penilaian Karyawan Terbaik')
@section('toolbar-status','false')

@section('navbar')
@include('layouts.navbar.navbar')
@endsection

@section('sidebar')
@include('layouts.navbar.sidebar')
@endsection

@section('content')
<div class="row justify-content-center mt-9">
    <div class="col-lg-12">
        <div class="row justify-content-center">
            <form id="kt_tambah_pemain_form" action="{{route('wp.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-12 mb-6">
                    <div class="card card-flush">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row align-items-center">
                                        <div class="col-lg-9 mb-6">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol rounded symbol-35px overflow-hidden me-5">
                                                    <div class="symbol-label bg-light-primary">
                                                        <i class="fa-solid fa-id-badge text-primary fs-3"></i>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="fs-4 fw-bold text-dark d-block">Data Penilaian</span>
                                                    <span class="fs-7 text-muted">Pengisian Penilaian Tiap Karyawan PT. Persada</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 mb-6">
                                            <label class="d-flex align-items-center fs-6 form-label mb-2">
                                                <span class="fw-bold required">Tanggal</span>
                                            </label>
                                            <input type="date" required  class="form-control form-control-solid" placeholder="" name="tanggal_test">
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 table-responsive mb-6">
                                    <table class="table table-bordered table-striped border table-rounded gy-5" id="kt_table_karyawan">
                                        <thead class="bg-primary text-white align-middle">
                                            <tr class="fw-bold fs-7 text-uppercase">
                                                <th class="text-center w-50px bg-black text-white">#</th>
                                                <th class="w-150px bg-black text-white text-start">Nama Karyawan</th>
                                                @foreach ($getKriteria as $gt)
                                                <th class="w-150px text-center">{{ $gt->nama_kriteria }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody class="fs-7">
                                            @php
                                            $index=1;
                                            @endphp
                                            @foreach ($getKaryawan as $gk)
                                            <tr>
                                                <td class="text-center">{{ $index++ }}</td>
                                                <td>
                                                    <input type="hidden" value="{{ $gk->id }}" name="karyawan_test[]">
                                                    {{ $gk->nama_karyawan }}
                                                </td>
                                                @foreach ($getKriteria as $gts)
                                                <td class="text-center">
                                                    <input type="hidden" value="{{ $gk->id }}" name="karyawan_id[]">
                                                    <input type="hidden" value="{{ $gts->id }}" name="criteria_id[]">
                                                    <select class=""required name="nilai_kriteria[]">
                                                        <option value="1">Sangat Rendah</option>
                                                        <option value="2">Rendah</option>
                                                        <option value="3">Cukup</option>
                                                        <option value="4">Tinggi</option>
                                                        <option value="5">Sangat Tinggi</option>
                                                    </select>
                                                </td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-5">
                                    <div class="notice align-items-center d-flex bg-light-danger rounded border-danger border border-dashed py-3 px-3">
                                        <i class="fa-solid fa-exclamation-circle fs-3 me-4 text-danger"></i>
                                        <div class="text-dark">
                                            Skala Penilaian : Sangat Rendah (1) - Sangat Tinggi (5)
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 text-end">
                                    <button type="submit" class="btn btn-sm btn-success w-lg-200px" data-kt-stepper-action="submit" id="btn_submit">
                                        <span class="indicator-label">Simpan</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document ).ready(function() {
        
    });
</script>

@endsection
