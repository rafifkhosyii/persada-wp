<div class="row mb-6">
    <div class="col-lg-12 mb-3">
        <label class="d-flex align-items-center fs-6 form-label mb-2">
            <span class="required fw-bold">Nama Kriteria</span>
        </label>
        <input type="text" class="form-control form-control-solid" placeholder="" required name="nama_kriteria">
        <div class="fv-plugins-message-container invalid-feedback"></div>
    </div>
    <div class="col-lg-6 mb-3">
        <label class="d-flex align-items-center fs-6 form-label mb-2">
            <span class="required fw-bold">Nilai Bobot Kriteria</span>
        </label>
        <input type="number" class="form-control form-control-solid" placeholder="" required name="nilai_bobot_awal">
        <div class="fv-plugins-message-container invalid-feedback"></div>
    </div>
    <div class="col-lg-6 mb-3">
        <div class="dataPilihan">
            
        </div>
    </div>
</div>