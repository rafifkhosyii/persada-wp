@extends('layouts.app')
@section('title','Kriteria Penilaian')

@section('navbar')
@include('layouts.navbar.navbar')
@endsection

@section('sidebar')
@include('layouts.navbar.sidebar')
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-12">
        <div class="row justify-content-center mt-9">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="d-flex align-items-center gap-2 mb-3 mb-md-0">
                            <div class="d-flex align-items-center">
                                <span class="fs-7 fw-bolder text-dark pe-4 text-nowrap d-none d-lg-block">List Kriteria</span>
                            </div>
                        </div>
                        <div class="card-toolbar">
                            <a href="#kt_modal_tambah_kriteria" data-bs-toggle="modal" class="btn btn-sm btn-primary btn_tambah_kriteria"><i class="fa-solid fa-plus me-1"></i>Tambah Kriteria</a>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table align-middle table-striped border table-rounded gy-5" id="kt_table_kriteria">
                                    <thead class="bg-primary">
                                        <tr class="fw-bold fs-7 text-white text-uppercase">
                                            <th class="text-center w-50px">#</th>
                                            <th class="">Nama Kriteria</th>
                                            <th class="text-center wp-50px">Bobot Awal</th>
                                            <th class="w-100px text-center">#</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fs-7">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>

@include('kriteria.add.modal-tambah-kriteria')
@include('kriteria.add.modal-edit-kriteria')
@include('kriteria.add.modal-hapus-kriteria')

<script>
    $(document ).ready(function() {
        
        window.tableDisc  = $('#kt_table_kriteria')
        .DataTable({
            processing: true,
            serverSide: true,
            retrieve: true,
            deferRender: true,
            responsive: false,
            aaSorting : [],
            ajax: {
                url : "{{route('criteria.get-table-criteria')}}"
            },
            language: {
                "lengthMenu": "Show _MENU_",
                "emptyTable" : "Tidak ada data terbaru 📁",
                "zeroRecords": "Data tidak ditemukan 😞",
            },
            dom:
            "<'row mb-2'" +
            "<'col-12 col-lg-6 d-flex align-items-center justify-content-start'l>" +
            "<'col-12 col-lg-6 d-flex align-items-center justify-content-lg-end justify-content-start 'f>" +
            ">" +
            
            "<'table-responsive'tr>" +
            
            "<'row'" +
            "<'col-12 col-lg-5 d-flex align-items-center justify-content-center justify-content-lg-start'i>" +
            "<'col-12 col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-end'p>" +
            ">",
            
            columns: [
            { data: 'DT_RowIndex'},
            { data: 'nama_kriteria'},
            { data: 'nilai_bobot_awal'},
            { data: 'action'},
            ],
            columnDefs: [
            {
                targets: 0,
                searchable : false,
                className: 'text-center',
            },
            {
                targets: 2,
                searchable : false,
                className: 'text-center',
            },
            {
                targets: -1,
                orderable : false,
                searchable : false,
                className : 'text-center',
            },
            ],
        });
        
        $('body').on('click', '.btn_tambah_kriteria', function () {
            var form_tambah = $('#kt_modal_tambah_kriteria_form')
            form_tambah.trigger("reset");
            $('.dataPilihan').html('');
            $('.dataPilihan').append(`
                <label class="d-flex align-items-center fs-6 form-label mb-2">
                <span class="required fw-bold">Tipe Kriteria</span>
            </label>
            <select class="drop-data form-select form-select-solid " required  required name="tipe_kriteria">
                <option value="1">Benefit</option>
                <option value="-1">Cost</option>
            </select>
            <div class="fv-plugins-message-container invalid-feedback"></div>
            `);
        });
        
        $("#kt_modal_tambah_kriteria_form").validate({
            messages: {
                nama_kriteria: {
                    required: "<span class='fw-semibold fs-8 text-danger'>Nama kriteria wajib diisi</span>",
                },
                nilai_bobot_kriteria: {
                    required: "<span class='fw-semibold fs-8 text-danger'>Nilai kriteria wajib diisi</span>",
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_tambah_kriteria_form').serialize(),
                    url: '{{route("criteria.store")}}',
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_tambah_kriteria_cancel').click();
                        var oTable = $('#kt_table_kriteria').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });
        
        $('table').on('click', '.btn_edit_kriteria', function () {
            var id = $(this).data('id')
            var form_edit = $('#kt_modal_edit_kriteria_form')
            form_edit.trigger("reset");
            $('.dataPilihan').html('');
            $.get(`{{url('')}}/persada-criteria/get-data/edit/criteria/${id}`, function (data) {
                form_edit.find("input[name='kriteria_id']").val(id)
                form_edit.find("input[name='nama_kriteria']").val(data.nama_kriteria)
                form_edit.find("input[name='nilai_bobot_awal']").val(data.nilai_bobot_awal)
                console.log(data.tipe_kriteria)
                if (data.tipe_kriteria==1) {
                    $('.dataPilihan').append(`
                <label class="d-flex align-items-center fs-6 form-label mb-2">
                <span class="required fw-bold">Tipe Kriteria</span>
            </label>
            <select class="drop-data form-select form-select-solid " required  required name="tipe_kriteria">
                <option selected value="1">Benefit</option>
                <option value="-1">Cost</option>
            </select>
            <div class="fv-plugins-message-container invalid-feedback"></div>
            `);   
                }else{
                    $('.dataPilihan').append(`
                <label class="d-flex align-items-center fs-6 form-label mb-2">
                <span class="required fw-bold">Tipe Kriteria</span>
            </label>
            <select class="drop-data form-select form-select-solid " required  required name="tipe_kriteria">
                <option selected value="-1">Cost</option>
                <option  value="1">Benefit</option>
            </select>
            <div class="fv-plugins-message-container invalid-feedback"></div>
            `);  
                }
            })
        });
        
        $("#kt_modal_edit_kriteria_form").validate({
            messages: {
                nama_kriteria: {
                    required: "<span class='fw-semibold fs-8 text-danger'>Nama kriteria wajib diisi</span>",
                },
                nilai_bobot_awal: {
                    required: "<span class='fw-semibold fs-8 text-danger'>Nilai kriteria wajib diisi</span>",
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_edit_kriteria_form').serialize(),
                    url: '{{route("criteria.update")}}',
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_edit_kriteria_cancel').click();
                        var oTable = $('#kt_table_kriteria').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });

        $('table').on('click', '.btn_hapus_kriteria', function () {
            var id = $(this).data('id')
            var form_hapus = $('#kt_modal_hapus_kriteria_form')
            form_hapus.trigger("reset");
            $.get(`{{url('')}}/persada-criteria/get-data/edit/criteria/${id}`, function (data) {
                form_hapus.find("input[name='criteria_id']").val(id)
            })
        });

        $("#kt_modal_hapus_kriteria_form").validate({
            submitHandler: function(form) {
                $.ajax({
                    data: $('#kt_modal_hapus_kriteria_form').serialize(),
                    url: '{{route("criteria.destroy")}}',
                    type: "DELETE",
                    dataType: 'json',
                    success: function (data) {
                        $('#kt_modal_hapus_kriteria_cancel').click();
                        var oTable = $('#kt_table_kriteria').dataTable();
                        oTable.fnDraw(false);
                        toastr.success(data.status,'Selamat !!');
                    },
                    error: function (xhr, status, errorThrown) {
                        toastr.error('Terjadi kesalahan' ,'Opps !!');
                    }
                });
            }
        });
        
    });
</script>
@endsection
