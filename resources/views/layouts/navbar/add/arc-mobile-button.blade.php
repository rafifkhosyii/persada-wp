<div class="app-navbar-item ms-4 d-lg-none" title="Show header menu">
    <div class="btn btn-icon btn-active-color-primary btn-light w-30px h-30px w-md-40px h-md-40px" id="kt_app_header_menu_toggle_mobile">
        <i class="fa-solid fa-bars-staggered"></i>
    </div>
</div>