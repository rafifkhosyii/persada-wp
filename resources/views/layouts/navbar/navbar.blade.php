@php
$firstLetter =  mb_substr(auth()->user()->name, 0, 1);
// $firstLetter =  'S';
@endphp
<div id="kt_app_header" class="app-header">
    <div class="app-container container-fluid d-flex align-items-stretch justify-content-between" id="kt_app_header_container">
        
        <div class="d-flex align-items-center d-lg-none ms-n3 me-3" title="Show header menu">
            <div class="btn btn-icon btn-light btn-active-color-primary w-35px h-35px" id="kt_app_sidebar_mobile_toggle">
                <i class="fa-solid fa-bars"></i>
            </div>
        </div>
        
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
            <a href="{{route('dashboard')}}" class="d-md-none">
                <img alt="Logo" src="{{asset('sense')}}/media/logos/logo-bfc.png" class="h-35px" />
            </a>
            <div class="page-title d-flex flex-column justify-content-center flex-wrap">
                <h1 class="page-heading d-flex text-dark fw-bolder fs-3 flex-column justify-content-center my-0" id="page_name">@yield('title')</h1>
                <span class="text-muted fs-7 fw-semibold">PT. Persada Balikpapan</span>
            </div>
        </div>
        
        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">
            <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="{default: 'append', lg: 'prepend'}" data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
                
            </div>
            <div class="app-navbar flex-shrink-0">
                <div class="app-navbar-item ms-4">
                    <a href="#" class="btn btn-primary btn-icon btn-sm" data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                        <span class="theme-light-show">
                            <i class="fs-2 fa-solid fa-sun"></i>
                        </span>
                        <span class="theme-dark-show ">
                            <i class="fs-2 fa-solid fa-moon"></i>
                        </span>
                    </a>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-muted menu-icon-muted menu-active-bg menu-state-color fw-semibold py-4 fs-base w-150px" data-kt-menu="true" data-kt-element="theme-mode-menu">
                        <div class="menu-item px-3 my-0">
                            <a href="#" class="menu-link px-3 py-2" data-kt-element="mode" data-kt-value="light">
                                <span class="menu-icon" data-kt-element="icon">
                                    <i class="fa-solid fa-sun"></i>
                                </span>
                                <span class="menu-title">Light</span>
                            </a>
                        </div>
                        <div class="menu-item px-3 my-0">
                            <a href="#" class="menu-link px-3 py-2" data-kt-element="mode" data-kt-value="dark">
                                <span class="menu-icon" data-kt-element="icon">
                                    <i class="fa-solid fa-moon"></i>
                                </span>
                                <span class="menu-title">Dark</span>
                            </a>
                        </div>
                        <div class="menu-item px-3 my-0">
                            <a href="#" class="menu-link px-3 py-2" data-kt-element="mode" data-kt-value="system">
                                <span class="menu-icon" data-kt-element="icon">
                                    <i class="fa-solid fa-desktop"></i>
                                </span>
                                <span class="menu-title">System</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center ms-4" id="kt_header_user_menu_toggle">
                    <div class="btn btn-flex align-items-center py-2 px-0" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                        <div class="symbol symbol-35px">
                            <div class="symbol-label fs-1 bg-light-primary text-primary">{{$firstLetter}}</div>
                        </div>
                    </div>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
                        <div class="menu-item px-3">
                            <div class="menu-content d-flex align-items-center px-3">
                                <div class="symbol symbol-50px me-5">
                                    <div class="symbol-label fs-1 bg-light-primary text-primary">{{$firstLetter}}</div>
                                </div>
                                <div class="d-flex flex-column" style="overflow-wrap:break-word;inline-size:170px">
                                    <span class="fw-bold align-items-center fs-6">{{substr(auth()->user()->name,0,10)}} ...</span>
                                    <span class="fw-semibold text-muted fs-8">{{substr(auth()->user()->email,0,15)}} ...</span>
                                </div>
                            </div>
                        </div>
                        <div class="separator my-2"></div>
                        <div class="menu-item px-5">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="menu-link px-5">
                                <span class="menu-title position-relative text-danger">Logout
                                    <span class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">
                                        <i class="fas fa-arrow-right text-danger"></i>
                                    </span>
                                </span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

