<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {   
        $admin = User::create([
            'name' => 'Admin/Spv',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12345678'),
        ]);
        $admin->assignRole('administrator');
        
        $staff = User::create([
            'name' => 'Karyawan/Staff',
            'email' => 'staff@gmail.com',
            'password' => bcrypt('12345678'),
        ]);
        $staff->assignRole('staff');
    }
}