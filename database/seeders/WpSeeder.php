<?php

namespace Database\Seeders;

use App\Models\Criteria;
use App\Models\NilaiS;
use App\Models\Player;
use App\Models\Wp;
use App\Models\WpPlayer;
use Illuminate\Database\Seeder;

class WpSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {

        //kriteria
        $kriteria = ['Tingkat Kedisiplinan','Tingkat Pelanggaran','Tingkat Prestasi','Aktif Ikut Serta Kegiatan KPwBI Balikpapan','Ketersediaan Melaksanakan Tugas Diluar Jam Kerja'];
        // $kriteria = ['Kedisiplinan Dalam Satu Periode Penilaian','Tingkat Pelanggaran Dalam Satu Periode Penilaian','Tingkat Prestasi Dalam Satu Periode Penilaian','Aktif Dalam Keikutsertaan Kegiatan Yang Diadakan Oleh KPwBI Balikpapan','Ketersediaan Dalam Melaksanakan Tugas Diluar Jam Kerja Sesuai Arahan'];
        $nilai_bobot_awal = [5,1,3,3,3];
        $tipe_kriteria = [1,-1,1,1,1];
        for ($i=0; $i < count($kriteria); $i++){
            $createCriteria = Criteria::create([
                'nama_kriteria'=>$kriteria[$i],
                'nilai_bobot_awal'=>$nilai_bobot_awal[$i],
                'tipe_kriteria'=>$tipe_kriteria[$i],
            ]);
        }

        //kriteria_normalisasi
        $sumBobotCriteria = Criteria::sum('nilai_bobot_awal');
        $getCriteria = Criteria::all();
        foreach ($getCriteria as $gC) {
            Criteria::where('id',$gC->id)->update([
                'nilai_normalisasi'=>($gC->nilai_bobot_awal/$sumBobotCriteria)*$gC->tipe_kriteria,
            ]);
        }

        //karyawan
        $karyawan = ['Ryan Tri Prayogo','Muhammad Zefri','I Putu Endra'];
        $nip_karyawan = ['1','2','3'];
        for($i=0; $i<count($karyawan); $i++){
            $createKaryawan = Player::create([
                'nama_karyawan'=>$karyawan[$i],
                'nip'=>$nip_karyawan[$i],
            ]);
        }
    }
}
