<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiSPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_s_players', function (Blueprint $table) {
            $table->id();
            $table->integer('wp_id');
            $table->integer('karyawan_id');
            $table->integer('criteria_id');
            $table->double('s_k_nilai')->default(0);
            // $table->double('s1_k_nilai')->default(0);
            // $table->double('s2_k_nilai')->default(0);
            // $table->double('s3_k_nilai')->default(0);
            // $table->double('s4_k_nilai')->default(0);
            // $table->double('s5_k_nilai')->default(0);
            // $table->double('nilai_s')->default(0);
            // $table->double('sum_s')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_s_players');
    }
}
