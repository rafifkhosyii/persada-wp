<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiVPlayersTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('nilai_v_players', function (Blueprint $table) {
            $table->id();
            $table->integer('wp_id');
            $table->integer('karyawan_id');
            $table->double('pow_s_k_nilai')->default(0);
            $table->double('sum_s_k_nilai')->default(0);
            $table->double('nilai_v_karyawan')->default(0);
            $table->timestamps();
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('nilai_v_players');
    }
}
