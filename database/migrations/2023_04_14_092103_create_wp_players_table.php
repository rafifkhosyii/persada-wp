<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWpPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wp_players', function (Blueprint $table) {
            $table->id();
            $table->integer('wp_id');
            $table->integer('karyawan_id');
            $table->integer('criteria_id');
            $table->integer('c_k_nilai')->default(1);
            $table->integer('bobot_kriteria')->default(0);
            $table->integer('tipe_kriteria')->default(0);
            $table->double('normalisasi_kriteria')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wp_players');
    }
}
