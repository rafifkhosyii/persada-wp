<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wp extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'wps';
    protected $guarded = [];
}
