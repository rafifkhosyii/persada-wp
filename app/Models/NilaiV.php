<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiV extends Model
{
    use HasFactory;
    protected $table = 'nilai_v_players';
    protected $guarded = [];
}
