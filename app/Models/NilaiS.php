<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiS extends Model
{
    use HasFactory;
    protected $table = 'nilai_s_players';
    protected $guarded = [];
}
