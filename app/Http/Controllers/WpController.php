<?php

namespace App\Http\Controllers;

use App\Models\Criteria;
use App\Models\NilaiS;
use App\Models\NilaiV;
use App\Models\Player;
use App\Models\Wp;
use App\Models\WpPlayer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class WpController extends Controller
{
    public function index()
    {
        return view('wp.index');
    }

    public function create()
    {
        $getKriteria = Criteria::where('deleted_at',null)->get();
        $getKaryawan = Player::where('deleted_at',null)->get();
        return view('wp.create', compact('getKriteria','getKaryawan'));
    }
    
    public function detail($id)
    {
        $getKaryawanTerbaik = DB::table('wps')
        ->join('players','players.id','wps.karyawan_id')
        ->where('wps.id',$id)
        ->select('wps.*','players.nama_karyawan')
        ->first();

        $kriteriaDipakai = DB::table('wp_players')
        ->join('criterias','criterias.id','wp_players.criteria_id')
        ->select('wp_players.*','criterias.nama_kriteria','criterias.nilai_bobot_awal',)
        ->groupBy('wp_players.criteria_id')
        ->where('wp_players.wp_id',$id)
        ->get();

        $karyawanDipakai = DB::table('wp_players')
        ->join('players','players.id','wp_players.karyawan_id')
        ->where('wp_players.wp_id',$id)
        ->groupBy('wp_players.karyawan_id')
        ->select('wp_players.*','players.nama_karyawan')
        ->get();

        $kriteriaDipakaiS = DB::table('nilai_s_players')
        ->join('criterias','criterias.id','nilai_s_players.criteria_id')
        ->select('nilai_s_players.*','criterias.nama_kriteria','criterias.nilai_bobot_awal',)
        ->groupBy('nilai_s_players.criteria_id')
        ->where('nilai_s_players.wp_id',$id)
        ->get();
        
        $karyawanDipakaiS = DB::table('nilai_s_players')
        ->join('players','players.id','nilai_s_players.karyawan_id')
        ->where('nilai_s_players.wp_id',$id)
        ->select('nilai_s_players.*','players.nama_karyawan')
        ->groupBy('nilai_s_players.karyawan_id')
        ->get();

        $nilaiVKaryawan = DB::table('nilai_v_players')
        ->join('players','players.id','nilai_v_players.karyawan_id')
        ->where('nilai_v_players.wp_id',$id)
        ->select('nilai_v_players.*','players.nama_karyawan')
        ->orderBy('nilai_v_players.nilai_v_karyawan','DESC','nilai_v_players.karyawan_id','DESC')
        ->get();

        return view('wp.detail', compact('getKaryawanTerbaik','kriteriaDipakaiS','karyawanDipakaiS','karyawanDipakai','id','kriteriaDipakai','nilaiVKaryawan'));
    }
    
    public function store(Request $request)
    {     
        //inisiasiKriteria 
        $getKriteria = Criteria::all();
        
        //Inisiasi WP
        $tahunTest = substr(date('Y', strtotime($request->tanggal_test)),-2);
        $kodeTest = 'WP'.rand(2,99).'-'.$tahunTest;
        $createWp = Wp::create([
            'tanggal_test'=>$request->tanggal_test,
            'kode_test'=>$kodeTest,
        ]);

        //createNilaiS&V
        for ($l=0; $l < count($request->karyawan_test); $l++) { 
            NilaiV::create([
                'wp_id'=>$createWp->id,
                'karyawan_id'=>$request->karyawan_test[$l],
            ]);
        }

        //nilaiWp Karyawan
        for ($i=0; $i < count($request->karyawan_id); $i++) { 
            
            $gbk = Criteria::where('id',$request->criteria_id[$i])->first();

            $nilaiK = WpPlayer::create([
                'wp_id'=>$createWp->id,
                'karyawan_id'=>$request->karyawan_id[$i],
                'criteria_id'=>$request->criteria_id[$i],
                'c_k_nilai'=>$request->nilai_kriteria[$i],
                'bobot_kriteria'=>$gbk->nilai_bobot_awal,
                'tipe_kriteria'=>$gbk->tipe_kriteria,
                'normalisasi_kriteria'=>$gbk->nilai_normalisasi,
            ]);
            
            //mengisiNilaiSKaraywan
            $nilaiS = NilaiS::create([
                'wp_id'=>$createWp->id,
                'karyawan_id'=>$request->karyawan_id[$i],
                'criteria_id'=>$request->criteria_id[$i],
                's_k_nilai'=>pow($request->nilai_kriteria[$i],$gbk->nilai_normalisasi),
            ]);

            //mengisiTotalNilaiSKaryawan
            $tempS=1;
            $cariNilaiS = NilaiS::where('wp_id',$createWp->id)->where('karyawan_id',$request->karyawan_id[$i])->get();
            foreach ($cariNilaiS as $cns) {
                $tempS *= $cns->s_k_nilai;
            }
            $STemp = NilaiV::where('wp_id',$createWp->id)->where('karyawan_id',$request->karyawan_id[$i])->update([
                'wp_id'=>$createWp->id,
                'karyawan_id'=>$request->karyawan_id[$i],
                'pow_s_k_nilai'=>$tempS,
                'sum_s_k_nilai'=> $cariNilaiS->sum('s_k_nilai'), 
            ]);
        }

        //totalNilaiS
            $getSKaryawan = NilaiV::where('wp_id',$createWp->id)->sum('pow_s_k_nilai');
            $updNilaiSWP = Wp::where('id',$createWp->id)->update([
                'total_nilai_s'=> $getSKaryawan,
            ]);

        //mengisiNilaiV
            for ($i=0; $i < count($request->karyawan_test); $i++) { 
                $getVKaryawan = NilaiV::where('wp_id',$createWp->id)->where('karyawan_id',$request->karyawan_test[$i])->first();
                $updateVKaryawan = NilaiV::where('wp_id',$createWp->id)->where('karyawan_id',$request->karyawan_test[$i])->update([
                    'nilai_v_karyawan'=>$getVKaryawan->pow_s_k_nilai/$getVKaryawan->sum_s_k_nilai,
                ]);
            }
        
        //karyawanTerbaik
        $getKT = NilaiV::where('wp_id',$createWp->id)->orderBy('nilai_v_karyawan','DESC','karyawan_id','DESC')->first();
        $updateKT = Wp::where('id',$createWp->id)->update([
            'karyawan_id'=>$getKT->karyawan_id,
            'nilai_v_karyawan_terbaik'=>$getKT->nilai_v_karyawan,
        ]);
        
        return redirect()->route('wp.index');
    }

    public function getTableWp(Request $request)
    {  
        if (request()->ajax()) {
            $query = DB::table('wps')
            ->join('players','players.id','wps.karyawan_id')
            ->select('wps.*','players.nama_karyawan','players.nip')
            ->where('wps.deleted_at',null)
            ->orderBy('wps.id','DESC')
            ;
            $query = $query->get();
            return DataTables::of($query)
            ->addColumn('nip', function ($nip) {
                return 'K'.$nip->nip;
            })
            ->addColumn('action', function ($action) {
                return '     
                <button type="button" class="btn btn-secondary btn-icon btn-sm" data-kt-menu-placement="bottom-end" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa-solid fa-ellipsis-vertical"></i></button>
                <ul class="dropdown-menu">
                <li><a href="'.route('wp.detail',['id'=>$action->id]).'" class="dropdown-item py-2"><i class="fa-solid fa-clipboard me-2"></i>Detail</a></li>
                <li><a href="#kt_modal_hapus_wp" data-bs-toggle="modal" data-id="'.$action->id.'" class="dropdown-item py-2 text-danger btn_hapus_wp"><i class="fa-solid fa-trash text-danger me-2"></i>Hapus</a></li>
                </ul>
                ';
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function destroy(Request $request)
    {
        try {
            Wp::where('id',$request->wp_id)->delete();
            return response()->json([
                "status" => "Penilaian berhasil dihapus",
            ]);
                
        }
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }
}
