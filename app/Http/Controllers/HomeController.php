<?php

namespace App\Http\Controllers;

use App\Models\Criteria;
use App\Models\Player;
use App\Models\Position;

class HomeController extends Controller
{
    public function index()
    {
        $totalPemain = Player::where('deleted_at',null)->count();
        $totalKriteria = Criteria::count();
        return view('home',compact('totalPemain','totalKriteria'));
    }
}
