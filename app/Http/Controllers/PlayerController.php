<?php

namespace App\Http\Controllers;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class PlayerController extends Controller
{
    public function index()
    {
        return view('player.index');
    }

    public function destroy(Request $request)
    {
        try {
            Player::where('id',$request->player_id)->delete();
            return response()->json([
                "status" => "Pemain berhasil dihapus",
            ]);
                
        }
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }

    public function getTablePlayer(Request $request)
    {  
        if (request()->ajax()) {
            $query = DB::table('players')
            ->where('players.deleted_at',null);
            $query = $query->get();
            return DataTables::of($query)
            ->addColumn('nip', function ($nip){
                return 'K'.$nip->nip;
            })
            ->addColumn('action', function ($action) {
                return '     
                <button type="button" class="btn btn-secondary btn-icon btn-sm" data-kt-menu-placement="bottom-end" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa-solid fa-ellipsis-vertical"></i></button>
                <ul class="dropdown-menu">
                <li><a href="#kt_modal_edit_karyawan" data-bs-toggle="modal" data-nama="'.$action->nama_karyawan.'" data-id="'.$action->id.'" class="dropdown-item py-2 btn_edit_karyawan"><i class="fa-solid fa-edit me-2"></i>Edit</a></li>
                <li><a href="#kt_modal_hapus_karyawan" data-bs-toggle="modal" data-id="'.$action->id.'" class="dropdown-item py-2 text-danger btn_hapus_karyawan"><i class="fa-solid fa-trash text-danger me-2"></i>Hapus</a></li>
                </ul>
                ';
            })
            ->addIndexColumn()
            ->rawColumns(['action','nip'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        $getPgw = Player::count();
        try {
            Player::create([
                'nama_karyawan'=>$request->nama_karyawan,
                'nip'=>$getPgw+1,
            ]);
            return response()->json([
                "status" => "Karyawan berhasil ditambahkan",
            ]);
        } 
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }
    
    public function update(Request $request)
    {
        try {
            Player::where('id',$request->karyawan_id)->update([
                'nama_karyawan'=>$request->nama_karyawan,
            ]);
            return response()->json([
                "status" => "Karyawan berhasil diperbaharui",
            ]);
        } 
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }

    public function getDataPlayer(Player $id)
    {  
        return response()->json($id);
    }
}
