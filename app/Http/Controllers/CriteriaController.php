<?php

namespace App\Http\Controllers;

use App\Models\Criteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class CriteriaController extends Controller
{
    public function index()
    {
        return view('kriteria.index');
    }

    public function store(Request $request)
    {
        try {
            $lastCriteria = Criteria::create([
                'nama_kriteria'=>$request->nama_kriteria,
                'nilai_bobot_awal'=>$request->nilai_bobot_awal,
                'tipe_kriteria'=>$request->tipe_kriteria,
            ]);
            $getKriteria = Criteria::where('deleted_at',null)->get();
            foreach ($getKriteria as $gk ) {
                $krteriaTerpilih = Criteria::where('id',$gk->id)->first();
                $updateNormalisasi = Criteria::where('id',$krteriaTerpilih->id)->update([
                    'nilai_normalisasi'=>$krteriaTerpilih->nilai_bobot_awal/($gk->tipe_kriteria*$getKriteria->sum('nilai_bobot_awal')),
                ]);
            }
            return response()->json([
                "status" => "Kriteria berhasil ditambahkan",
            ]);
        } 
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }

    public function update(Request $request)
    {
        try {
            Criteria::where('id',$request->kriteria_id)->update([
                'nama_kriteria'=>$request->nama_kriteria,
                'nilai_bobot_awal'=>$request->nilai_bobot_awal,
                'tipe_kriteria'=>$request->tipe_kriteria,
            ]);
            $getKriteria = Criteria::where('deleted_at',null)->get();
            foreach ($getKriteria as $gk ) {
                $krteriaTerpilih = Criteria::where('id',$gk->id)->first();
                $updateNormalisasi = Criteria::where('id',$krteriaTerpilih->id)->update([
                    'nilai_normalisasi'=>$krteriaTerpilih->nilai_bobot_awal/($gk->tipe_kriteria*$getKriteria->sum('nilai_bobot_awal')),
                ]);
            }
            return response()->json([
                "status" => "Kriteria berhasil diperbaharui",
            ]);
        } 
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }

    public function getDataCriteria(Criteria $id)
    {
        return response()->json($id);
    }

    public function getTableCriteria(Request $request)
    {  
        if (request()->ajax()) {
            $query = DB::table('criterias')
            ->select('criterias.*')
            ->where('criterias.deleted_at',null)
            ->orderBy('criterias.id','DESC');
            $query = $query->get();
            return DataTables::of($query)
            ->addColumn('action', function ($action) {
                return '     
                <button type="button" class="btn btn-secondary btn-icon btn-sm" data-kt-menu-placement="bottom-end" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa-solid fa-ellipsis-vertical"></i></button>
                <ul class="dropdown-menu">
                <li><a href="#kt_modal_edit_kriteria" data-bs-toggle="modal" data-id="'.$action->id.'" class="dropdown-item py-2 btn_edit_kriteria"><i class="fa-solid fa-edit me-2"></i>Edit</a></li>
                <li><a href="#kt_modal_hapus_kriteria" data-bs-toggle="modal" data-id="'.$action->id.'" class="dropdown-item py-2 text-danger btn_hapus_kriteria"><i class="fa-solid fa-trash text-danger me-2"></i>Hapus</a></li>
                </ul>
                ';
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function destroy(Request $request)
    {
        try {
            Criteria::where('id',$request->criteria_id)->delete();
            $getKriteria = Criteria::where('deleted_at',null)->get();
            foreach ($getKriteria as $gk ) {
                $krteriaTerpilih = Criteria::where('id',$gk->id)->first();
                $updateNormalisasi = Criteria::where('id',$krteriaTerpilih->id)->update([
                    'nilai_normalisasi'=>$krteriaTerpilih->nilai_bobot_awal/($gk->tipe_kriteria*$getKriteria->sum('nilai_bobot_awal')),
                ]);
            }
            return response()->json([
                "status" => "Kriteria berhasil dihapus",
            ]);
            
                
        }
        catch (\Throwable $th) {
            Log::error($th);
            return response()->json("Oppss !!, Terjadi kesalahan", 500);
        }
    }
}
