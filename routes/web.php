<?php

use App\Http\Controllers\CriteriaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\WpController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('dashboard');
    });
    
    Route::controller(WpController::class)->group(function () {
        Route::prefix('persada-wp')->group(function () {
            Route::get('/','index')->name('wp.index');
            Route::get('/create','create')->name('wp.create');
            Route::get('/detail/{id}','detail')->name('wp.detail');

            Route::post('/store','store')->name('wp.store');
            Route::delete('/destroy','destroy')->name('wp.destroy');

            Route::get('/get-data/table/wp','getTableWp')->name('wp.get-table-wp');
        });
    });
    
    Route::controller(CriteriaController::class)->group(function () {
        Route::prefix('persada-criteria')->group(function () {
            Route::get('/','index')->name('criteria.index');

            Route::post('/store','store')->name('criteria.store');
            Route::post('/update','update')->name('criteria.update');
            Route::delete('/destroy','destroy')->name('criteria.destroy');

            Route::get('/get-data/table/criteria','getTableCriteria')->name('criteria.get-table-criteria');
            Route::get('/get-data/edit/criteria/{id}','getDataCriteria')->name('criteria.get-data-criteria');
        });
    });
    
    Route::controller(PlayerController::class)->group(function () {
        Route::prefix('persada-player')->group(function () {
            Route::get('/','index')->name('player.index');
            
            Route::post('/store','store')->name('player.store');
            Route::post('/update','update')->name('player.update');
            Route::delete('/destroy','destroy')->name('player.destroy');

            Route::get('/get-data/table/player','getTablePlayer')->name('player.get-table-player');
            Route::get('/get-data/data/player/{id}','getDataPlayer')->name('player.get-data-player');
        });
    });

});

require __DIR__ . '/auth.php';
